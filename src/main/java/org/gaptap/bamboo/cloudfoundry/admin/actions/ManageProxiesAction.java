/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.admin.actions;

import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.atlassian.bamboo.ww2.aware.permissions.GlobalAdminSecurityAware;
import org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryAdminService;
import org.gaptap.bamboo.cloudfoundry.admin.InUseByTargetException;
import org.gaptap.bamboo.cloudfoundry.admin.Proxy;
import org.gaptap.bamboo.cloudfoundry.admin.Target;
import com.google.common.collect.Lists;

/**
 * @author David Ehringer
 */
@SuppressWarnings({ "serial", "unchecked" })
public class ManageProxiesAction extends BaseManagementAction implements GlobalAdminSecurityAware {

	private static final String DEFAULT_PORT = "80";

	private int proxyId;
	private String host;
	private String port;
	private String name;
	private String description;
	
	private List<Target> targetsUsingProxy = Lists.newArrayList();

	private final CloudFoundryAdminService adminService;

	public ManageProxiesAction(CloudFoundryAdminService adminService) {
		this.adminService = adminService;
	}

	@Override
	public String doAdd() {
		String result = super.doAdd();
		port = DEFAULT_PORT;
		return result;
	}

	public String doCreate() throws Exception {
		if (!isValidProxy()) {
			setMode(ADD);
			addActionError(getText("cloudfoundry.global.errors.task.validation.errors"));
			return ERROR;
		}
		adminService.addProxy(name, host, Integer.parseInt(port), description);
		return SUCCESS;
	}

	public String doEdit() {
		setMode(EDIT);
		Proxy proxy = adminService.getProxy(proxyId);
		name = proxy.getName();
		description = proxy.getDescription();
		host = proxy.getHost();
		port = String.valueOf(proxy.getPort());
		return EDIT;
	}

	public String doUpdate() throws Exception {
		if (!isValidProxy()) {
			setMode(EDIT);
			addActionError(getText("cloudfoundry.global.errors.task.validation.errors"));
			return ERROR;
		}
		// TODO detect issues
		adminService.updateProxy(proxyId, name, host, Integer.parseInt(port), description);
		return SUCCESS;
	}

	private boolean isValidProxy() throws Exception {
		if (StringUtils.isBlank(name)) {
			addFieldError("name", getText("cloudfoundry.global.required.field"));
		}
		if (StringUtils.isBlank(host)) {
			addFieldError("host", getText("cloudfoundry.global.required.field"));
		}
		if (StringUtils.isBlank(port)) {
			addFieldError("port", getText("cloudfoundry.global.required.field"));
		}
		try {
			int portInt = Integer.parseInt(port);
			if (portInt < 1) {
				addFieldError("port", getText("cloudfoundry.proxy.port.validation.tooSmall"));
			}
		} catch (NumberFormatException e) {
			addFieldError("port", getText("cloudfoundry.proxy.port.validation.integer"));
		}
		return !(hasFieldErrors() || hasActionErrors());
	}

	public String doDelete() {
		try {
			adminService.deleteProxy(proxyId);
		} catch (InUseByTargetException e) {
			targetsUsingProxy.addAll(e.getTargetsUsing());
			// provide data to display
			doEdit();
			return ERROR;
		}
		return SUCCESS;
	}

	public Collection<Proxy> getProxies() {
		return adminService.allProxies();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getProxyId() {
		return proxyId;
	}

	public void setProxyId(int proxyId) {
		this.proxyId = proxyId;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public List<Target> getTargetsUsingProxy() {
		return targetsUsingProxy;
	}

	public void setTargetsUsingProxy(List<Target> targetsUsingProxy) {
		this.targetsUsingProxy = targetsUsingProxy;
	}
	
}
