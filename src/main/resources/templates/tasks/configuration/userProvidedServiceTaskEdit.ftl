[#import "/lib/ace.ftl" as ace ]

[@ww.text name="cloudfoundry.task.userservice.description"][/@ww.text]

[#include "selectTargetFragment.ftl"]

[@ui.bambooSection titleKey='cloudfoundry.task.userservice.section']

    [@ww.textfield labelKey='cloudfoundry.task.userservice.name' name='cf_servicename' required='true' /]

	[@ww.select labelKey='cloudfoundry.task.userservice.data.options' name='cf_dataOption'
	                                        listKey='key' listValue='value' toggle='true'
	                                        list=dataOptions /]

	[@ui.bambooSection dependsOn="cf_dataOption" showOn="inline"]
		[@ace.textarea labelKey='cloudfoundry.task.userservice.inline' name="cf_inlineData" required=true/]
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn="cf_dataOption" showOn="file"]
		[@ww.textfield labelKey='cloudfoundry.task.userservice.file' descriptionKey='cloudfoundry.task.userservice.file.description' name='cf_file' required='true' cssClass="long-field" /]
	[/@ui.bambooSection]

    [@ww.textfield labelKey='cloudfoundry.task.userservice.syslogUrl' descriptionKey='cloudfoundry.task.userservice.syslogUrl.description' name='cf_syslog_url' /]
    [@ww.textfield labelKey='cloudfoundry.task.userservice.routeServiceUrl' descriptionKey='cloudfoundry.task.userservice.routeServiceUrl.description' name='cf_route_service_url' /]

    [@ww.checkbox labelKey='cloudfoundry.task.userservice.create.ignoreTaskOnAlreadyExists' name='cf_createIgnoreIfExists' /]
    
[/@ui.bambooSection]
